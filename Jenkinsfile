def IMAGE_TAG = "UNKNOWN"

pipeline {
  agent {
    kubernetes {
      defaultContainer 'jnlp'
      yaml """
apiVersion: v1
kind: Pod
metadata:
  labels:
    run: jnlp
    sidecar.istio.io/inject: 'false'
spec:
  serviceAccountName: jenkins-agent
  containers:
  - name: git
    image: gcr.io/cloud-builders/git
    command:
    - cat
    tty: true
  - name: busybox
    image: busybox
    command:
    - cat
    tty: true
  - name: helm
    image: dtzar/helm-kubectl:3.13.1
    command:
    - cat
    tty: true
  - name: kaniko
    image: gcr.io/kaniko-project/executor:debug
    imagePullPolicy: Always
    command:
    - /busybox/cat
    tty: true
    volumeMounts:
    - name: jenkins-docker-cfg
      mountPath: /kaniko/.docker
  volumes:
  - name: jenkins-docker-cfg
    projected:
      sources:
      - secret:
          name: regcred
          items:
          - key: .dockerconfigjson
            path: config.json
"""
   }
}
    environment {
        PATH = "/busybox:/kaniko:$PATH"
    }
    stages {
        stage ('Pre Actions-Build Started') {
            steps {
              script {
                IMAGE_TAG = sh(returnStdout: true, script: 'date +%Y-%m-%dT%H-%M-%SZ').trim()
              }
              echo "Hey, your CI/CD trigger has *Started* \n*Trigger: * `${env.JOB_NAME}`#${env.BUILD_NUMBER}\n${env.BUILD_URL}\n*GitHub: * ${GIT_BRANCH}\nTag: ${IMAGE_TAG} >> ${GIT_URL}"
            }
        }
        stage ('git clone - master') {
            when {
                branch 'master'
            }
            steps{
                container('git'){
                    git branch: 'master',
                        url: 'https://gitlab.com/xeedio/podinfo.git'
                }
            }
        }
        stage ('git clone - staging') {
            when {
                branch 'staging'
            }
            steps{
                container('git'){
                    git branch: 'staging',
                        url: 'https://gitlab.com/xeedio/podinfo.git'
                }
            }
        }
        stage ('git clone - prod') {
            when {
                branch 'prod'
            }
            steps{
                container('git'){
                    git branch: 'prod',
                        url: 'https://gitlab.com/xeedio/podinfo.git'
                }
            }
        }
        stage('Feature Branch Build') {
            when {
                expression { BRANCH_NAME != 'master' && BRANCH_NAME != 'staging' && BRANCH_NAME != 'prod'}
            }
            steps{
                container('git'){
                    git branch: '${BRANCH_NAME}',
                        url: 'https://gitlab.com/xeedio/podinfo.git'
                }
            }
        }
        stage ('building docker image - master') {
            when {
                branch 'master'
            }
            steps {
                container(name: 'kaniko', shell: '/busybox/sh') {
                    sh """#!/busybox/sh
                      /kaniko/executor \
                        --dockerfile Dockerfile \
                        --context `pwd`/ \
                        --verbosity info \
                        --log-format text \
                        --build-arg REVISION=dev.$IMAGE_TAG \
                        --destination registry.gitlab.com/xeedio/podinfo:dev.$IMAGE_TAG
                    """
                }
            }
        }
        stage ('building docker image - staging') {
            when {
                branch 'staging'
            }
            steps {
                container(name: 'kaniko', shell: '/busybox/sh') {
                    sh """#!/busybox/sh
                      /kaniko/executor \
                        --dockerfile Dockerfile \
                        --context `pwd`/ \
                        --verbosity info \
                        --log-format text \
                        --build-arg REVISION=staging.$IMAGE_TAG \
                        --destination registry.gitlab.com/xeedio/podinfo:staging.$IMAGE_TAG
                    """
                }
            }
        }
        stage ('building docker image - prod') {
            when {
                branch 'prod'
            }
            steps {
                container(name: 'kaniko', shell: '/busybox/sh') {
                    sh """#!/busybox/sh
                      /kaniko/executor \
                        --dockerfile Dockerfile \
                        --context `pwd`/ \
                        --verbosity info \
                        --log-format text \
                        --build-arg REVISION=prod.$IMAGE_TAG \
                        --destination registry.gitlab.com/xeedio/podinfo:prod.$IMAGE_TAG
                    """
                }
            }
        }
        stage ('deploy to k8s - master') {
            when {
                branch 'master'
            }
            steps {
                container('helm'){
                    sh """
                      helm repo add podinfo https://stefanprodan.github.io/podinfo
                      helm repo update
                      helm upgrade --install podinfo podinfo/podinfo \
                        --namespace dev \
                        --set image.repository=registry.gitlab.com/xeedio/podinfo \
                        --set image.tag=dev.$IMAGE_TAG \
                        --set redis.enabled=true \
                        --set replicaCount=2 \
                        --set ui.color="#34577c" \
                        --set ui.message="Greetings from dev.$IMAGE_TAG"
                    """
                }
            }
        }
        stage ('deploy to k8s - staging') {
            when {
                branch 'staging'
            }
            steps {
                container('helm'){
                    sh """
                      helm repo add podinfo https://stefanprodan.github.io/podinfo
                      helm repo update
                      helm upgrade --install podinfo podinfo/podinfo \
                        --namespace staging \
                        --set image.repository=registry.gitlab.com/xeedio/podinfo \
                        --set image.tag=staging.$IMAGE_TAG \
                        --set redis.enabled=true \
                        --set replicaCount=2 \
                        --set ui.color="#34577c" \
                        --set ui.message="Greetings from staging.$IMAGE_TAG"
                    """
                }
            }
        }
        stage ('deploy to k8s - prod') {
            when {
                branch 'prod'
            }
            steps {
                container('helm'){
                    sh """
                      helm repo add podinfo https://stefanprodan.github.io/podinfo
                      helm repo update
                      helm upgrade --install podinfo podinfo/podinfo \
                        --namespace prod \
                        --set image.repository=registry.gitlab.com/xeedio/podinfo \
                        --set image.tag=prod.$IMAGE_TAG \
                        --set redis.enabled=true \
                        --set replicaCount=2 \
                        --set ui.color="#34577c" \
                        --set ui.message="Greetings from prod.$IMAGE_TAG"
                    """
                }
            }
        }
    }
    post {
        success {
            echo "Hurray! CI/CD is *Success* \n*Trigger: * `${env.JOB_NAME}`#${env.BUILD_NUMBER}\n${env.BUILD_URL}\n*GitHub: * ${GIT_BRANCH}\nTag: ${IMAGE_TAG} >> ${GIT_URL}"
        }
        failure {
            echo "Oops, something's wrong; CI/CD *Failed* \n*Trigger: * `${env.JOB_NAME}`#${env.BUILD_NUMBER}\n${env.BUILD_URL}\n*GitHub: * ${GIT_BRANCH}\nTag: ${IMAGE_TAG} >> ${GIT_URL}"
        }
    }
}
